package com.shenmou.petwater.android.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import com.shenmou.petwater.android.R;
import com.shenmou.petwater.android.bean.WaterIntakeBean;
import com.shenmou.petwater.android.utils.DateUtil;
import com.shenmou.petwater.android.utils.DpUtil;

import java.util.List;

/**
 * @author Vincent Vincent
 * @version v1.0
 * @name EasyApp
 * @page com.vincent.easyapp.view
 * @class describe
 * @date 2018/9/14 14:31
 */
public class WaterIntakeDetailView extends BaseScrollerView {

    public WaterIntakeDetailView(Context context,  AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        mViewHeight = h;
        mViewWidth = w;
        super.onSizeChanged(w, h, oldw, oldh);
    }
    private static final String TAG = "用水量";
    private float mViewHeight;
    private float mViewWidth;
    private float mSmaillGridHeight = 15;//背景小格子的宽度
    private float mSmaillGridWidth = 20;//背景小格子的宽度
    private Paint mSmailGridPaint;//小格子画笔
    private Context mContext;
    private float mStartX;
    private float mStartY;
    private float mStartYMarginBottom = 30;//起点Y距离底部边距
    private float mMaxValue;//表最大值
    private float mMinValue = 0;//表最小值 表示从0开始
    private List<WaterIntakeBean> data;
    private float mDataMargin = 10;//数据与数据之间的间隔
    private float mItemDataWidth = 25;//数据柱状图的宽度
    private float mMaxOffsetX = 0;//X轴最大偏移量
    private Paint mRectPaint;//数据柱状图
    private String date;//当前日期
    private WaterIntakeBean waterIntakeBean;
    private Paint mDateTextPaint;//日期
    private Rect mDateRect;
    private Rect mValueRect;
    private Paint mValuePaint;//数值画笔
    private float mMaxTopMargin = 30;//最大的数据距离View顶部的距离

    /**
     * 设置数据
     *
     * @param data
     */
    public void setData(List<WaterIntakeBean> data) {
        this.data = data;
        mMaxValue = 0;
        for (int i = 0; i < data.size(); i++) {
            if (mMaxValue < data.get(i).getWaterIntake()) {
                mMaxValue = data.get(i).getWaterIntake();
            }
        }
        invalidate();
    }

    //初始化控件
    private void init(Context context, AttributeSet attrs) {
        this.mContext = context;

        mSmaillGridHeight = DpUtil.dp2px(mContext, mSmaillGridHeight);
        mSmaillGridWidth = DpUtil.dp2px(mContext, mSmaillGridWidth);
        mStartYMarginBottom = DpUtil.dp2px(mContext, mStartYMarginBottom);
        mDataMargin = DpUtil.dp2px(mContext, mDataMargin);
        mItemDataWidth = DpUtil.dp2px(mContext, mItemDataWidth);
        mMaxTopMargin = DpUtil.dp2px(mContext,mMaxTopMargin);

        mDateRect = new Rect();
        mValueRect = new Rect();

        mSmailGridPaint = new Paint();
        mSmailGridPaint.setColor(ContextCompat.getColor(mContext, R.color.color_green_02EAAB));
        mSmailGridPaint.setAntiAlias(true);

        mRectPaint = new Paint();
        mRectPaint.setColor(ContextCompat.getColor(mContext, R.color.color_green_02EAAB));
        mRectPaint.setAntiAlias(true);

        mDateTextPaint = new Paint();
        mDateTextPaint.setColor(ContextCompat.getColor(mContext, R.color.color_green_02EAAB));
        mDateTextPaint.setAntiAlias(true);
        mDateTextPaint.setTextSize(DpUtil.dp2px(mContext,11));

        mValuePaint = new Paint();
        mValuePaint.setColor(ContextCompat.getColor(mContext, R.color.color_black_545454));
        mValuePaint.setAntiAlias(true);
        mValuePaint.setTextSize(DpUtil.dp2px(mContext,11));


    }

    @Override
    protected void onDrawContent(Canvas mCanvas) {
        mCanvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_OVER);
        drawBg(mCanvas);
        drawTable(mCanvas);
    }

    private void drawTable(Canvas mCanvas) {
        if (data != null && data.size() > 0) {
            //绘制将超过屏幕
            if (mMaxOffsetX == 0) {
                mMaxOffsetX = (mItemDataWidth + mDataMargin) * data.size() + mDataMargin - mViewWidth;
                setMaxVal((int) mMaxOffsetX);
            }
            if (mDataMargin + (mDataMargin + mItemDataWidth) * data.size() > mViewWidth) {
                mCanvas.translate(mOffsetX, 0);//滑动
            }
            Path path = new Path();
            RectF rectF = new RectF();
            path.moveTo(mDataMargin, 0);//绘制第一个数据之前留下一个空白
            path.lineTo(mDataMargin, 0);
            for (int i = 0; i < data.size(); i++) {
                waterIntakeBean = data.get(i);
                float x = mDataMargin + (mDataMargin + mItemDataWidth) * i;
                float y = getYForValue(waterIntakeBean);
                rectF.bottom = mStartY;
                rectF.left = x;
                rectF.right = x + mItemDataWidth;
                rectF.top = y;
                mCanvas.drawRect(rectF, mRectPaint);
                //绘制日期
                date = DateUtil.getDateString(DateUtil.DATE_FORMAT_MONTH_DAY_2, waterIntakeBean.getTime());
                mDateTextPaint.getTextBounds(date,0,date.length(),mDateRect);
                mCanvas.drawText(date,x + mItemDataWidth/2 - mDateRect.width()/2,mViewHeight - mStartYMarginBottom/2 - mDateRect.height()/2,mDateTextPaint);
                String valueStr = String.valueOf(waterIntakeBean.getWaterIntake());
                mValuePaint.getTextBounds(valueStr,0,valueStr.length(),mValueRect);
                mCanvas.drawText(valueStr,x + mItemDataWidth/2 - mValueRect.width()/2,y - mValueRect.height() - DpUtil.dp2px(mContext,2),mValuePaint);
            }
        }
    }

    private float getYForValue(WaterIntakeBean waterIntakeBean) {
        float item = waterIntakeBean.getWaterIntake() / mMaxValue;
//        Log.d(TAG, "getYForValue: "+mMaxValue + " " + waterIntakeBean.getWaterIntake() + " "+ item);
        //这里的20 是控件顶部预留20的数值控件
        return mMaxTopMargin + ((mStartY - mMaxTopMargin) * (1 - item));
    }

    /**
     * 绘制背景
     *
     * @param mCanvas
     */
    private void drawBg(Canvas mCanvas) {
        mStartY = mViewHeight - mStartYMarginBottom;
        //绘制x方向上的横线
        int xNum = (int) (mStartY / mSmaillGridHeight);
        for (int i = 0; i < xNum + 1; i++) {
            mCanvas.drawLine(0, mSmaillGridHeight * i, mViewWidth, mSmaillGridHeight * i, mSmailGridPaint);
        }
        int yNum = (int) (mViewWidth / mSmaillGridWidth);
        float yRemainder = mStartY % mSmaillGridHeight;
        mStartY = mStartY - yRemainder;
        for (int i = 0; i < yNum + 1; i++) {
            mCanvas.drawLine(mSmaillGridWidth * i, 0, mSmaillGridWidth * i, mStartY, mSmailGridPaint);
        }
    }


    /**
     * 绘制 自定义View的 wrap_content 可做模板
     * @param widthMeasureSpec
     * @param heightMeasureSpec
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        if (widthMode == MeasureSpec.AT_MOST && heightMode == MeasureSpec.AT_MOST){
            setMeasuredDimension((int) mViewWidth,(int)mViewHeight);
        }else if (widthMode == MeasureSpec.AT_MOST){
            setMeasuredDimension((int)mViewWidth,heightSize);
        }else if(heightMode == MeasureSpec.AT_MOST){
            setMeasuredDimension(widthSize,(int)mViewHeight);
        }
    }

}
