package com.shenmou.petwater.android.base;

/**
 * @author Vincent Vincent
 * @version v1.0
 * @name NewXmPluginSDK-master
 * @page com.shenmou.petwater.android.base
 * @class describe
 * @date 2018/9/14 9:52
 */
public interface ICommonDataInterface {

    void workTime(String time);

}
