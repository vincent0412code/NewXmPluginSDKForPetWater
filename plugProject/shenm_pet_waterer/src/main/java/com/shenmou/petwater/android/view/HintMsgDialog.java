package com.shenmou.petwater.android.view;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.shenmou.petwater.android.R;
import com.shenmou.petwater.android.utils.ScreenUtils;

/**
 * @author Vincent Vincent
 * @version v1.0
 * @name NewXmPluginSDK-master
 * @page com.shenmou.petwater.android.view
 * @class describe
 * @date 2018/9/15 15:36
 */
public class HintMsgDialog extends BaseDialog {

    private TextView tvContent;
    private TextView tvLeft;
    private TextView tvRight;

    private String strContent;
    private String strLeft;
    private String strRight;
    private Context mContext;

    private HintMsgDialogClickListener dialogClickListener;

    public HintMsgDialog setDialogClickListener(HintMsgDialogClickListener dialogClickListener) {
        this.dialogClickListener = dialogClickListener;
        return this;
    }

    public HintMsgDialog(Context context) {
        super(context);
        this.mContext = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_layout_hint_msg);
        tvContent = findViewById(R.id.dialog_hint_msg_title_tv);
        tvLeft = findViewById(R.id.dialog_hint_msg_left_tv);
        tvRight = findViewById(R.id.dialog_hint_msg_right_tv);
        setWidth(ScreenUtils.getScreenWidth(mContext)/3*2);
        setOutCancel(false);
        tvContent.setText(strContent);
        tvLeft.setText(strLeft);
        tvRight.setText(strRight);
        tvLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dialogClickListener != null)dialogClickListener.onLeftClick();
                dismiss();
            }
        });
        tvRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dialogClickListener != null)dialogClickListener.onRightClick();
                dismiss();
            }
        });
    }

    public HintMsgDialog setStrContent(String strContent) {
        this.strContent = strContent;
        if(tvContent != null)tvContent.setText(strContent);
        return this;
    }

    public HintMsgDialog setStrLeft(String strLeft) {
        this.strLeft = strLeft;
        if(tvLeft != null)tvLeft.setText(strLeft);
        return this;
    }

    public HintMsgDialog setStrRight(String strRight) {
        this.strRight = strRight;
        if(tvRight != null)tvRight.setText(strRight);
        return this;
    }

    public interface HintMsgDialogClickListener{

        void onLeftClick();

        void onRightClick();

    }

}
