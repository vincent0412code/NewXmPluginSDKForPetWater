package com.shenmou.petwater.android.widget;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.shenmou.petwater.android.R;
import com.shenmou.petwater.android.base.AppConfig;
import com.shenmou.petwater.android.base.BaseActivity;
import com.shenmou.petwater.android.view.HintMsgDialog;

/**
 * @author Vincent Vincent
 * @version v1.0
 * @name NewXmPluginSDK-master
 * @page com.shenmou.petwater.android.widget
 * @class describe
 * @date 2018/9/14 10:09
 */
public class HintMsgActivity extends BaseActivity {

    private String type;
    private TextView tvTitle;
    private TextView tvChange;
    private HintMsgDialog hintMsgDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hint_msg);
        mHostActivity.setTitleBarPadding(findViewById(R.id.view_root_hint_msg));
        type = getIntent().getStringExtra(AppConfig.INTENT_TYPE);
        initView();
        setTitleText(type);
    }

    private void initView() {
        tvTitle = (TextView) findViewById(R.id.hint_msg_tv_title);
        tvChange = (TextView)findViewById(R.id.hint_msg_tv_change);
        findViewById(R.id.hint_msg_finish_iv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //弹出框 提示
                finish();
            }
        });
        tvChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (type){
                    case AppConfig.INTENT_TYPE_CHANGE_WATER:
                        //换水提醒
                        showHintDialog(1);
                        break;
                    case AppConfig.INTENT_TYPE_FILTER_ELEMENT:
                        //换滤芯提醒
                        showHintDialog(2);
                        break;
                    case AppConfig.INTENT_TYPE_SUBMERGED_PUMP:
                        //潜水泵
                        showHintDialog(3);
                        break;
                }
            }
        });
    }

    private void setTitleText(String type) {
        switch (type){
            case AppConfig.INTENT_TYPE_CHANGE_WATER:
                tvTitle.setText(getString(R.string.string_common_change_water_remind));
                tvChange.setText(getString(R.string.string_hint_msg_change));
                break;
            case AppConfig.INTENT_TYPE_FILTER_ELEMENT:
                tvTitle.setText(getString(R.string.string_common_filter_element_remind));
                tvChange.setText(getString(R.string.string_hint_msg_change_filter_element));
                break;
            case AppConfig.INTENT_TYPE_SUBMERGED_PUMP:
                tvTitle.setText(getString(R.string.string_common_sinking_pump_remind));
                tvChange.setText(getString(R.string.string_hint_msg_change_submerged_pump));
                break;
        }
    }


    private void showHintDialog(final int type){
        hintMsgDialog = new HintMsgDialog(HintMsgActivity.this);
        hintMsgDialog.setStrContent(getString(R.string.dialog_string_finish_change))
                .setStrLeft(getString(R.string.dialog_string_no))
                .setStrRight(getString(R.string.dialog_string_yes))
                .setDialogClickListener(new HintMsgDialog.HintMsgDialogClickListener() {
            @Override
            public void onLeftClick() {

            }

            @Override
            public void onRightClick() {
                switch (type){
                    case 1:
                        //换水完成
                        break;
                    case 2:
                        //滤芯完成
                        break;
                    case 3:
                        //潜水泵完成
                        break;
                }
            }
        });
        if(!hintMsgDialog.isShowing()){
            hintMsgDialog.show();
        }
    }

}
