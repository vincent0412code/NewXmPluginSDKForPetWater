package com.shenmou.petwater.android.base;

/**
 * @author Vincent Vincent
 * @version v1.0
 * @name NewXmPluginSDK-master
 * @page com.shenmou.petwater.android.base
 * @class describe
 * @date 2018/9/14 9:44
 */
public class AppConfig {
    /**
     * 跳转类型
     */
    public static final String INTENT_TYPE = "intent_type";
    /**
     * 换水提醒
     */
    public static final String INTENT_TYPE_CHANGE_WATER = "intent_type_change_water";
    /**
     * 滤芯
     */
    public static final String INTENT_TYPE_FILTER_ELEMENT = "intent_type_filter_element";
    /**
     * 潜水泵
     */
    public static final String INTENT_TYPE_SUBMERGED_PUMP = "intent_type_submerged_pump";

    /**
     * 工作是简单保存
     */
    public static final String SHARED_WORK_TIME = "shared_work_time";

}
