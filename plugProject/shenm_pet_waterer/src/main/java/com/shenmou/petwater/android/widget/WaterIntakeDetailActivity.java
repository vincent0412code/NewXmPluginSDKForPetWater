package com.shenmou.petwater.android.widget;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.shenmou.petwater.android.R;
import com.shenmou.petwater.android.base.BaseActivity;
import com.shenmou.petwater.android.view.WaterIntakeDetailView;

/**
 * @author Vincent Vincent
 * @version v1.0
 * @name NewXmPluginSDK-master
 * @page com.shenmou.petwater.android.widget
 * @class describe
 * @date 2018/9/14 14:08
 */
public class WaterIntakeDetailActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_water_intake);
        mHostActivity.setTitleBarPadding(findViewById(R.id.title_bar));
        initView();
    }

    private TextView title;

    private WaterIntakeDetailView widvTable;

    private void initView() {
        widvTable = (WaterIntakeDetailView) findViewById(R.id.widv_table);
        title = (TextView)findViewById(R.id.title_bar_title);
        title.setText(getString(R.string.string_pet_water_intake_title));
        findViewById(R.id.title_bar_more).setVisibility(View.GONE);
        findViewById(R.id.title_bar_redpoint).setVisibility(View.GONE);
        findViewById(R.id.title_bar_return).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


}
