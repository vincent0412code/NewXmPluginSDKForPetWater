package com.shenmou.petwater.android.widget;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.shenmou.petwater.android.base.AppConfig;
import com.shenmou.petwater.android.base.BaseActivity;
import com.shenmou.petwater.android.R;
import com.shenmou.petwater.android.base.ICommonDataInterface;
import com.shenmou.petwater.android.view.wheel.adapter.ArrayWheelAdapter;
import com.shenmou.petwater.android.view.wheel.lib.WheelView;

import java.util.ArrayList;

/**
 * @author Vincent Vincent
 * @version v1.0
 * @name NewXmPluginSDK-master
 * @page com.shenmou.petwater.android.widget
 * @class describe
 * @date 2018/9/13 16:20
 */
public class WorkTimeSelectActivity extends BaseActivity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_select);
        // 设置titlebar在顶部透明显示时的顶部padding
        mHostActivity.setTitleBarPadding(findViewById(R.id.view_root_work_time));
        initView();
    }
    private static final String TAG = "工作时间";
    private ICommonDataInterface dataInterface;
    private String startTime;
    private String endTime;
    private String time;

    private LinearLayout llSave;
    private WheelView startHour, startMin, endHour, endMin;
    private ArrayList<String> hours = new ArrayList<>();
    private ArrayList<String> mins = new ArrayList<>();

    private void initView() {

        initHourData();
        initMinData();
        findViewById(R.id.work_select_finish_iv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        llSave = (LinearLayout) findViewById(R.id.work_select_save_ll);
        startHour = (WheelView) findViewById(R.id.movement_target_wv_list_hour_start);
        startMin = (WheelView) findViewById(R.id.movement_target_wv_list_min_start);
        endHour = (WheelView) findViewById(R.id.movement_target_wv_list_hour_end);
        endMin = (WheelView) findViewById(R.id.movement_target_wv_list_min_end);
        startHour.setCyclic(false);
        startMin.setCyclic(false);
        endHour.setCyclic(false);
        endMin.setCyclic(false);
        startHour.setAdapter(new ArrayWheelAdapter(hours));
        startMin.setAdapter(new ArrayWheelAdapter(mins));
        endHour.setAdapter(new ArrayWheelAdapter(hours));
        endMin.setAdapter(new ArrayWheelAdapter(mins));
        findViewById(R.id.work_select_save_ll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringBuilder startTimeBuilder = new StringBuilder();
                startTimeBuilder.append(String.valueOf(hours.get(startHour.getCurrentItem())));
                startTimeBuilder.append(":");
                startTimeBuilder.append(String.valueOf(mins.get(startMin.getCurrentItem())));

                StringBuilder endTimeBuilder = new StringBuilder();
                endTimeBuilder.append(String.valueOf(hours.get(endHour.getCurrentItem())));
                endTimeBuilder.append(":");
                endTimeBuilder.append(String.valueOf(mins.get(endMin.getCurrentItem())));

                startTime = startTimeBuilder.toString();
                endTime = endTimeBuilder.toString();
                Log.d(TAG, "onClick: "+startTime + " "+endTime);
                time = startTime + "～" + endTime;
                MainActivity.getSpUtil().putString(AppConfig.SHARED_WORK_TIME,time);
                finish();
            }
        });
    }

    private void initMinData() {
        for (int i = 0;i<60;i++){
            if(i<10){
                mins.add("0"+String.valueOf(i));
            }else {
                mins.add(String.valueOf(i));
            }
        }
    }

    private void initHourData() {
        for(int i = 0;i<24;i++){
            if(i<10){
                hours.add("0"+String.valueOf(i));
            }else {
                hours.add(String.valueOf(i));
            }
        }

    }


}
