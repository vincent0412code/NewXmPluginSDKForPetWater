package com.shenmou.petwater.android.view.wheel.listener;


public interface OnItemSelectedListener {
    void onItemSelected(int index);
}
