package com.shenmou.petwater.android.base;

import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.shenmou.petwater.android.utils.MyToastUtil;
import com.xiaomi.smarthome.device.api.XmPluginBaseActivity;

/**
 * @author Vincent Vincent
 * @version v1.0
 * @name NewXmPluginSDK-master
 * @page com.shenmou.petwater.android
 * @class describe
 * @date 2018/9/12 11:37
 */
public class BaseActivity extends XmPluginBaseActivity {


    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);

    }

    /**
     * 修改系统状态栏字体的颜色
     * @param isBlack
     */
    public void setSystemStatusTextColor(boolean isBlack){
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            if (isBlack) {
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//设置状态栏黑色字体
            }else {
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);//恢复状态栏白色字体
            }
        }
    }

    /**
     * 注意此方法需要在setContentView之前调用
     * @param colorId
     */
    public void setSystemStatusColor(int colorId){
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP){
            //兼容4.4
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            ViewGroup decorViewGroup = (ViewGroup) window.getDecorView();
            View statusBarView = new View(window.getContext());
            int statusBarHeight = getStatusBarHeight();
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, statusBarHeight);
            params.gravity = Gravity.TOP;
            statusBarView.setLayoutParams(params);
            statusBarView.setBackgroundColor(ContextCompat.getColor(BaseActivity.this, colorId));
            decorViewGroup.addView(statusBarView);
        }else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(BaseActivity.this, colorId));
        }
    }


    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    /**
     * 弹出toast消息
     * @param msg
     */
    public void toastMsg(String msg){
        if(TextUtils.isEmpty(msg)){
            return;
        }
        MyToastUtil.showMsg(BaseActivity.this,msg);
    }

}
