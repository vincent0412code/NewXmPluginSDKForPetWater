package com.shenmou.petwater.android.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.shenmou.petwater.android.R;
import com.shenmou.petwater.android.utils.DpUtil;

/**
 * @author Vincent Vincent
 * @version v1.0
 * @name EasyApp
 * @page com.vincent.easyapp.view
 * @class describe
 * @date 2018/9/11 16:44
 */
public class WaterInfoView extends View {

    public WaterInfoView(Context context) {
        super(context);
        init(context);
    }

    public WaterInfoView(Context context,  AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }
    private static final String TAG = "啊啊啊";
    private Context mContext;
    private float mViewWidth;
    private float mViewHeight;
    private String mTopTitle = "水质状态";
    private float mTitleMargin = 10;
    private Paint mTitlePaint;
    private Paint mValuePaint;
    private Paint mValueTagPaint;
    private Paint mBottomStatusPaint;
    private boolean drawBottomStatusBg = true;
    private int value = 0;
    //默认状态
    private String bottomTag = "设备离线";
    private String valueTag = "TDS";
    private Paint mLinePaint;
    // true 饮水量  false  水质
    private boolean drinkingWater = true;

    /**
     * true 宠物饮水量  false  水质
     * @param drinkingWater
     */
    public void setDrinkingWater(boolean drinkingWater) {
        this.drinkingWater = drinkingWater;
        invalidate();
    }

    public void setValueTag(String valueTag) {
        this.valueTag = valueTag;
        invalidate();
    }



    /**
     * 是否绘制底部状态文字的背景边框
     * @param drawBottomStatusBg true  绘制外边框  false 不绘制
     */
    public void setDrawBottomStatusBg(boolean drawBottomStatusBg) {
        this.drawBottomStatusBg = drawBottomStatusBg;
    }

    public void setBottomTag(String bottomTag) {
        this.bottomTag = bottomTag;
        invalidate();
    }

    public void setValue(int value) {
        this.value = value;
        invalidate();
    }

    public void setmTopTitle(String mTopTitle) {
        this.mTopTitle = mTopTitle;
        invalidate();
    }

    private void init(Context context) {
        this.mContext = context;
        mTitleMargin = DpUtil.dp2px(mContext,mTitleMargin);


        mTitlePaint = new Paint();
        mTitlePaint.setTextSize(DpUtil.dp2px(mContext,15));
        mTitlePaint.setColor(Color.parseColor("#545454"));
        mTitlePaint.setAntiAlias(true);

        mValuePaint = new Paint();
        mValuePaint.setTextSize(DpUtil.dp2px(mContext,50));
        mValuePaint.setAntiAlias(true);
        mValuePaint.setColor(ContextCompat.getColor(mContext, R.color.color_green_02EAAB));

        mValueTagPaint = new Paint();
        mValueTagPaint.setTextSize(DpUtil.dp2px(mContext,22));
        mValueTagPaint.setAntiAlias(true);
        mValueTagPaint.setColor(ContextCompat.getColor(mContext, R.color.color_green_02EAAB));


        mBottomStatusPaint = new Paint();
        mBottomStatusPaint.setTextSize(DpUtil.dp2px(mContext,16));
        mBottomStatusPaint.setAntiAlias(true);
        mBottomStatusPaint.setStyle(Paint.Style.STROKE);
        mBottomStatusPaint.setColor(ContextCompat.getColor(mContext, R.color.color_yellow_E6A93B));
        mBottomStatusPaint.setStrokeWidth(DpUtil.dp2px(mContext,1));

        mLinePaint = new Paint();
        mLinePaint.setAntiAlias(true);
        mLinePaint.setStrokeWidth(DpUtil.dp2px(mContext,1));
        mLinePaint.setColor(ContextCompat.getColor(mContext, R.color.color_yellow_E6A93B));

    }

    private Rect valueRect = new Rect();
    private String valueStr;

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mValuePaint.setTextSize(DpUtil.dp2px(mContext,50));
        valueStr = getStr(value);
        mValuePaint.getTextBounds(valueStr,0,valueStr.length(),valueRect);
        drawValue(valueStr,valueRect,canvas);
        drawTopText(canvas,valueRect);
        drawBottomText(canvas,valueRect);
        drawBottomLine(canvas);
    }
    //绘制最底部的线条
    private void drawBottomLine(Canvas canvas) {
        if(drinkingWater){
            mLinePaint.setColor(ContextCompat.getColor(mContext,R.color.color_yellow_E6A93B));
        }else {
            mLinePaint.setColor(ContextCompat.getColor(mContext,R.color.color_gray_c6c6c6));
        }
        canvas.drawLine(mViewWidth / 2 - DpUtil.dp2px(mContext,35),mViewHeight - DpUtil.dp2px(mContext,2),
                mViewWidth/2 - DpUtil.dp2px(mContext,5),mViewHeight - DpUtil.dp2px(mContext,2),mLinePaint);
        if(drinkingWater){
            mLinePaint.setColor(ContextCompat.getColor(mContext,R.color.color_gray_c6c6c6));
        }else {
            mLinePaint.setColor(ContextCompat.getColor(mContext,R.color.color_yellow_E6A93B));
        }
        canvas.drawLine(mViewWidth / 2 + DpUtil.dp2px(mContext,5),mViewHeight - DpUtil.dp2px(mContext,2),
                mViewWidth/2 + DpUtil.dp2px(mContext,35),mViewHeight - DpUtil.dp2px(mContext,2),mLinePaint);
    }

    private void drawBottomText(Canvas canvas, Rect valueRect) {
        Rect bottomStatusRect = new Rect();
        mBottomStatusPaint.getTextBounds(bottomTag,0,bottomTag.length(),bottomStatusRect);
        float y = mViewHeight - (mViewHeight /2 - valueRect.height()/2 - bottomStatusRect.height())/2 - bottomStatusRect.height()/2;
        canvas.drawText(bottomTag,mViewWidth/2 - bottomStatusRect.width()/2,
                y + bottomStatusRect.height()/2,mBottomStatusPaint);
       if(drawBottomStatusBg){
            //绘制底部状态文字的背景
            RectF rectF = new RectF();
            rectF.set(mViewWidth/2 - bottomStatusRect.width()/2  - DpUtil.dp2px(mContext,10),
                    y - bottomStatusRect.height()/2 - DpUtil.dp2px(mContext,5),
                    mViewWidth/2 + bottomStatusRect.width()/2 + DpUtil.dp2px(mContext,10),
                    y + bottomStatusRect.height()/2 + DpUtil.dp2px(mContext,7));
            canvas.drawRoundRect(rectF,bottomStatusRect.height() * 2,bottomStatusRect.height() * 2,mBottomStatusPaint);
        }

    }

    /**
     * 绘制顶部的文字
     * @param canvas
     * @param valueRect
     */
    private void drawTopText(Canvas canvas, Rect valueRect) {
         Rect mTitleRect = new Rect();
        mTitlePaint.getTextBounds(mTopTitle,0,mTopTitle.length(),mTitleRect);
        canvas.drawText(mTopTitle,mViewWidth /2 - mTitleRect.width()/2,
                (mViewHeight/2 - valueRect.height()/2 - mTitleRect.height())/2+valueRect.height()/2,mTitlePaint);
    }

    /**
     * 根据值获取文本
     * @param value
     * @return
     */
    private String getStr(int value) {
        if(value <= 0){
            return "000";
        }else if(value < 10){
            return "00"+String.valueOf(value);
        }else if(value < 100){
            return "0"+String.valueOf(value);
        }else {
            return String.valueOf(value);
        }
    }

    /**
     * 绘制值
     * @param valueStr
     * @param valueRect
     * @param canvas
     */
    private void drawValue(String valueStr, Rect valueRect, Canvas canvas) {
        float y = mViewHeight/2 + valueRect.height()/2;
        if(value > 99){
            //直接绘制
            mValuePaint.setAlpha(255);
            canvas.drawText(String.valueOf(value),mViewWidth/2 - valueRect.width()/2,y,mValuePaint);
        }else if (value > 9){
            mValuePaint.setAlpha((int)(255 * 0.37));
            canvas.drawText("0",mViewWidth/2 - valueRect.width()/2,y,mValuePaint);
            mValuePaint.setAlpha(255);
            canvas.drawText(String.valueOf(value),mViewWidth/2 - valueRect.width()/2 + valueRect.width()/3,y,mValuePaint);
        }else if(value == 0){
            mValuePaint.setAlpha((int)(255 * 0.37));
            canvas.drawText("000",mViewWidth/2 - valueRect.width()/2,y,mValuePaint);
        }else {
            mValuePaint.setAlpha((int)(255 * 0.37));
            canvas.drawText("00",mViewWidth/2 - valueRect.width()/2,y,mValuePaint);
            mValuePaint.setAlpha(255);
            canvas.drawText(String.valueOf(value),mViewWidth/2 - valueRect.width()/2 + valueRect.width()/3  * 2 + DpUtil.dp2px(mContext,4),y,mValuePaint);
        }
        mValuePaint.setTextSize(DpUtil.dp2px(mContext,15));
        canvas.drawText(valueTag,mViewWidth /2 + valueRect.width()/2 + DpUtil.dp2px(mContext,10),y,mValuePaint);
        Log.e(TAG, "drawValue: "+String.valueOf((mViewWidth /2 + valueRect.width()/2 + DpUtil.dp2px(mContext,10))));
        Log.e(TAG, "drawValue: " + String.valueOf(mViewWidth/2) + " " + String.valueOf(valueRect.width()/2) + " "+valueStr);
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        mViewWidth = w;
        mViewHeight = h;
        super.onSizeChanged(w, h, oldw, oldh);
    }
}
