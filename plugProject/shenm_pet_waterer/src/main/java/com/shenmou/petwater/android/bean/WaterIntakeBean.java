package com.shenmou.petwater.android.bean;

import org.litepal.crud.LitePalSupport;

/**
 * @author Vincent Vincent
 * @version v1.0
 * @name NewXmPluginSDK-master
 * @page com.shenmou.petwater.android.bean
 * @class describe
 * @date 2018/9/14 14:02
 */
public class WaterIntakeBean extends LitePalSupport{

    private long time;
    private int waterIntake;

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public int getWaterIntake() {
        return waterIntake;
    }

    public void setWaterIntake(int waterIntake) {
        this.waterIntake = waterIntake;
    }
}
