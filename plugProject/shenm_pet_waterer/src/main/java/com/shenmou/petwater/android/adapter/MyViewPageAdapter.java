package com.shenmou.petwater.android.adapter;


import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * @author Vincent Vincent
 * @version v1.0
 * @name MeiLiZhi
 * @page com.mlizhi.kingdom.adapter
 * @class describe
 * @date 2018/9/5 15:31
 */
public class MyViewPageAdapter extends PagerAdapter {

    private List<View> views;

    public MyViewPageAdapter(List<View> views){
        this.views = views;
    }

    @Override
    public int getCount() {
        return views == null ?0:views.size();
    }

    @Override
    public Object instantiateItem( ViewGroup container, int position) {
        container.addView(views.get(position),0);
//        return super.instantiateItem(container, position); //UnsupportedOperationException: Required method instantiateItem was not overridden
        //每次滑动的时候把视图添加到viewpager
        return views.get(position);
    }

    @Override
    public void destroyItem( ViewGroup container, int position,  Object object) {
        container.removeView(views.get(position));
    }

    @Override
    public boolean isViewFromObject( View view,  Object object) {
        return view == object;
    }

}
