package com.shenmou.petwater.android.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import com.shenmou.petwater.android.R;
import com.shenmou.petwater.android.utils.DpUtil;

/**
 * @author Vincent Vincent
 * @version v1.0
 * @name NewXmPluginSDK-master
 * @page com.shenmou.petwater.android.view
 * @class describe
 * @date 2018/9/14 10:32
 */
public class HintTextView extends View {

    public HintTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private Context mContext;
    private int mViewWidth;
    private int mViewHeight;
    private int day = 0;

    private String text1;
    private String text2;
    private String text3;

    private Paint mPaint;
    private Paint mDayPaint;

    public void setText1(String text1) {
        this.text1 = text1;
    }

    public void setText2(String text2) {
        this.text2 = text2;
    }

    public void setText3(String text3) {
        this.text3 = text3;
    }

    public void setDay(int day) {
        this.day = day;
        invalidate();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        mViewWidth = w;
        mViewHeight = h;
        super.onSizeChanged(w, h, oldw, oldh);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        text1 = mContext.getString(R.string.string_hint_text_1);
        Rect rect1 = new Rect();
        mPaint.getTextBounds(text1,0,text1.length(),rect1);
        canvas.drawText(text1,mViewWidth/2 - rect1.width()/2,rect1.height(),mPaint);

    }

    private void init(Context context) {
        this.mContext = context;
        mPaint = new Paint();
        mPaint.setTextSize(DpUtil.dp2px(mContext,14));
        mPaint.setColor(ContextCompat.getColor(mContext, R.color.color_black_545454));

        mDayPaint = new Paint();
        mDayPaint.setTextSize(DpUtil.dp2px(mContext,40));
        mPaint.setColor(ContextCompat.getColor(mContext, R.color.color_green_01C88D));

    }

    /**
     * 绘制 自定义View的 wrap_content 可做模板
     * @param widthMeasureSpec
     * @param heightMeasureSpec
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        if (widthMode == MeasureSpec.AT_MOST && heightMode == MeasureSpec.AT_MOST){
            setMeasuredDimension(mViewWidth,mViewHeight);
        }else if (widthMode == MeasureSpec.AT_MOST){
            setMeasuredDimension(mViewWidth,heightSize);
        }else if(heightMode == MeasureSpec.AT_MOST){
            setMeasuredDimension(widthSize,mViewHeight);
        }
    }

}
