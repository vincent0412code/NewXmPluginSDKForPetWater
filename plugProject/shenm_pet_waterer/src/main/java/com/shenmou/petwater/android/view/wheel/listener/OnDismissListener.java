package com.shenmou.petwater.android.view.wheel.listener;

/**
 * Created by Sai on 15/8/9.
 */
public interface OnDismissListener {
    public void onDismiss(Object o);
}
